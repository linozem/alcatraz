#include <iostream>
#include <vector>
#include <assert.h>

// Insert the new element into the provided vector, maintaining
// sorted order
void add_number (std::vector<float>& vec, float new_num) {
    int i = 0;
    while (vec[i] <= new_num && i != vec.size()) { i++; }
    vec.insert(vec.begin() + i, new_num);
}

// Return the median, where if we have an even number of
// elements we average the middle two
float const get_median (std::vector<float>& vec) {
    if (vec.size() == 0) return 0;
    
    if (vec.size() % 2 == 0) {
        // We want indicies to round down, but need to prevent it for vector values
        int low_index = (vec.size() - 1) / 2;
        float low_val = (float)vec[low_index];
        float high_val = (float)vec[low_index + 1];
        return (low_val + high_val) / 2;
    }
    return vec[vec.size()/2.0];
}

// Helper for testing
void const print_vector (std::vector<float>& vec) {
    for (int i = 0; i < vec.size(); i++) {
        std::cout << vec[i] << " ";
    }
    std::cout << std::endl;
}

int main(int argc, const char * argv[]) {
    // Do some basic testing.
    // Computing the median with an odd number of elements.
    std::vector<float> input {1, 2, 3, 4, 5};
    print_vector(input);
    assert(get_median(input) == 3);

    // Inserting at the end/median with an even number of elements.
    add_number(input, 6);
    print_vector(input);
    assert(get_median(input) == 3.5);
    
    // Inserting at the beginning.
    add_number(input, 0);
    print_vector(input);
    assert(get_median(input) == 3);
    
    // All the same.
    input = {3, 3, 3, 3, 3, 3};
    assert(get_median(input) == 3);
    
    // Empty.
    input = {};
    assert(get_median(input) == 0);
    
    // Floats.  Epsilon is configurable.
    float eps = 0.001;
    input = {4.5, 5.5, 6.5};
    assert(get_median(input) - 5.5 < eps);
    
    // Even number of floats.
    input = {4.5, 5.5, 6.5, 7.5};
    assert(get_median(input) - 6.0 < eps);
    
    std::cout << "Done testing\n";
    return 0;
}
